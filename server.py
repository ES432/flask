from flask import Flask
from flask import (request,
                   redirect,
                   url_for,
                   session,
                   render_template)

app = Flask(__name__)
app.secret_key="asdfasdfasd"
PASSWORD="topsecret"


@app.route("/hello")
def hello():
    return "Hello World!"

@app.route("/")
def root():
    return redirect(url_for("index"))

@app.route("/index")
def index():
    if not 'logged_in' in session or session['logged_in']:
        print("ARGGG! not logged in!!")
        return redirect(url_for('login'))
    else:
        return render_template('about.html',name="Sammy")


@app.route("/about")
def about():
    return render_template('about.html', name="John")

@app.route("/login",methods=['GET','POST'])
def login():
    if request.method == 'POST':
        print("got a form submission!")
        print(request.form['password'])
        if(request.form['password']==PASSWORD):
            session['logged_in'] = True
            return redirect(url_for('index'))
        else:
            return render_template('login.html',error=True)
    else:
        return render_template('login.html',error=False)
    
